/* bootinfo.c - command to make Emulab bootinfo call to boss server */

/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2008  Free Software Foundation, Inc.
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/normal.h>
#include <grub/dl.h>
#include <grub/err.h>
#include <grub/misc.h>
#include <grub/time.h>
#include <grub/term.h>
#if (!defined(__powerpc__) && !defined(__ppc__))
#include <grub/machine/memory.h>
#endif
#include <grub/mm.h>
#include <grub/device.h>
#include <grub/disk.h>
#include <grub/fs.h>
#include <grub/file.h>
#include <grub/net.h>
#include <grub/net/udp.h>
#include <grub/partition.h>
#include <grub/msdos_partition.h>
#include <grub/bootwhat.h>
#include <grub/extcmd.h>

GRUB_MOD_LICENSE ("GPLv3+");

#define	USER_ABORT		-2
#define MAX_CMD_LENGTH		512 /* XXX Is this sane? */
#define MAX_CMD_ARGS		64

#define DHCP_SNAME_OFFSET	44
#define DHCP_FILE_OFFSET	108
#define DHCP_OPTIONS_OFFSET	236

#define DHCP_OPTION_PAD		0
#define DHCP_OPTION_VENDOR	43
#define DHCP_OPTION_OVERLOAD	52
#define DHCP_OPTION_VCI         60
#define DHCP_OPTION_END		255

#define EMULAB_VCI              "Emulab"

#define DHCP_OPTION_EMULAB_BOOTINFO     128

#define GET_KEY_SLEEP_INTERVAL	100

//static int debug = 0;

typedef	grub_uint32_t	in_addr_t;
#define htonl(x)	(grub_cpu_to_be32(x))
#define ntohl(x)	(grub_be_to_cpu32(x))
#define htons(x)	(grub_cpu_to_be16(x))

#if 0
static void print_packet(char *buffer, int length)
{
	int i;

	for (i = 0; i < length; i++) {
		if (i % 8 == 0) {
			grub_printf("\n");
		}
		grub_printf("%02x ", buffer[i]);
	}
}
#endif

/* get a key, timing out after at least timeout milliseconds */
static int get_key(int timeout)
{
	int key = GRUB_TERM_NO_KEY;

	if (timeout < 0) {
		return grub_getkey();
	}

	while (timeout > 0) {
		key = grub_getkey_noblock();
		if (key != GRUB_TERM_NO_KEY)
			break;

		grub_millisleep(GET_KEY_SLEEP_INTERVAL);
		timeout -= GET_KEY_SLEEP_INTERVAL;
	}

	return key;
}

/* IPv4-only version of inet_pton */
static int pton(const char *string, in_addr_t *addr)
{
	grub_uint32_t o1, o2, o3, o4;
	const char *q;
	char *p;

	/* GRUB doesn't have anything useful like scanf(),
	 * so strtoul will have to work
	 */

	q = string;
	o1 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o2 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o3 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '.') {
		return -1;
	}

	q = p + 1;
	o4 = grub_strtoul(q, &p, 10);
	if (q == p || *p != '\0') {
		return -1;
	}

	/* make sure no bits > 7 are set */
	if ((o1 > 255) || (o2 > 255) ||
	    (o3 > 255) || (o4 > 255)) {
		return -1;
	}

	*addr = htonl(o1 << 24 | o2 << 16 | o3 << 8 | o4);

	return 0;
}

/* IPv4-only version of inet_ntop */
static const char *ntop(const in_addr_t *src, char *dest, int size)
{
	in_addr_t addr = ntohl(*src);

	grub_snprintf(dest, size, "%d.%d.%d.%d",
		      addr >> 24 & 0xff,
		      addr >> 16 & 0xff,
		      addr >>  8 & 0xff,
		      addr >>  0 & 0xff);

	return dest;
}

/* IPv4-only version of inet_ntoa */
static const char *ntoa(const in_addr_t addr)
{
	static char buf[16]; /* XXX.XXX.XXX.XXX\0 */
	grub_uint32_t ip = ntohl(addr);

	grub_snprintf(buf, sizeof(buf), "%d.%d.%d.%d",
		      (ip >> 24) & 0xff, (ip >> 16) & 0xff,
		      (ip >> 8) & 0xff, (ip >> 0) & 0xff);

	return buf;
}

static int decode_emulab_vendor_options(void *vendor_options, grub_size_t length, in_addr_t *ip)
{
	grub_uint8_t *p = vendor_options;
	int rc = 0;

	while (p < (grub_uint8_t *)vendor_options + length) {
		grub_uint8_t code, len = 0;

		code = *p;

		if (code == DHCP_OPTION_PAD) {
			p++;
		} else if (code == DHCP_OPTION_END) {
			break;
		} else {
			len = *(p + 1);
			p += 2;
			if (code == DHCP_OPTION_EMULAB_BOOTINFO) {
				if (len >= 4)
					*ip = *(in_addr_t *)p;
			}

			p += len;
		}
	}

	return rc;
}

static in_addr_t find_bootinfo_ip(void *dhcpdata, grub_size_t dhcplen)
{
	in_addr_t server_id = 0;
	int option_overload = 0;
	grub_uint8_t type, length;
	char *vendor_options = NULL, *vendor_class_id = NULL;
	int vendor_options_length = 0, vendor_class_id_len = 0;
	struct grub_net_bootp_packet *bp = dhcpdata;

	/* Start looking through the DHCP options
	 * for the Server Identifier option.  Options
	 * start at offset 240 (after the DHCP magic
	 * cookie).
	 */

	grub_printf("Checking DHCP data for bootinfo server address...");
	char *p = (char *)dhcpdata + DHCP_OPTIONS_OFFSET + 4; /* 4 for the cookie */
	do {
		if (p > (char *)dhcpdata + dhcplen) {
			break;
		}

		if (option_overload & 2) {
			/* 'file' field used for options */
			p = (char *)dhcpdata + DHCP_FILE_OFFSET;
			option_overload &= 1;
		}
		else if (option_overload & 1) {
			/* 'sname' field used for options */
			p = (char *)dhcpdata + DHCP_SNAME_OFFSET;
			option_overload = 0;
		}
		while (*(grub_uint8_t *)p != DHCP_OPTION_END) {
			type = *(grub_uint8_t *)p;

			if (type == DHCP_OPTION_PAD)
				length = 1;
			else
				length = *(grub_uint8_t *)(p + 1);

			if (type == DHCP_OPTION_OVERLOAD && length == 1) {
				option_overload = *(grub_uint8_t *)(p + 2);
			} else if (type == DHCP_OPTION_VENDOR && length >= 1) {
				if (!vendor_options) {
					vendor_options = p + 2;
					vendor_options_length = length;
				}
			} else if (type == DHCP_OPTION_VCI && length >= 1) {
				if (!vendor_class_id) {
					vendor_class_id = p + 2;
					vendor_class_id_len = length;
				}
			}

			p += length + 2;
		}
	} while (option_overload);

	if (vendor_class_id && (grub_strncmp(vendor_class_id, EMULAB_VCI,
	                                    vendor_class_id_len) == 0 ||
	                        grub_strncmp(vendor_class_id, "PXEClient",
	                                     9) == 0)) {
		if (vendor_options) {
			decode_emulab_vendor_options(vendor_options,
			                             vendor_options_length,
			                             &server_id);
			if (server_id)
				grub_printf("found Emulab bootinfo option, server=%s\n",
					    ntoa(server_id));
		}
	}

	if (server_id == 0) {
		server_id = (in_addr_t)bp->server_ip;
		grub_printf("not found, using DHCP server %s\n",
			    ntoa(server_id));
	}

	return server_id;
}

struct recv_data {
	boot_info_t *reply;
	grub_size_t rlen;
	int error;
	int stop;
};

/*
 * XXX we always return zero rather than an error so that we will continue
 * to poll. This allows us to get past transient errors. The last error
 * will be returned in the error field of the struct in the event of an
 * eventual timeout.
 */
static grub_err_t
bootinfo_receive (grub_net_udp_socket_t sock __attribute__ ((unused)),
		  struct grub_net_buff *nb, void *_data)
{
	struct recv_data *data = _data;
	boot_info_t *binfo = (void *) nb->data;
	grub_size_t plen;
	grub_err_t err = GRUB_ERR_NONE;

	plen = nb->tail - nb->data;
	if (plen < 2*sizeof(grub_uint32_t) ||
	    plen > sizeof(*data->reply) ||
	    binfo->version != BIVERSION_CURRENT ||
	    !(binfo->opcode == BIOPCODE_BOOTWHAT_REPLY ||
	      binfo->opcode == BIOPCODE_BOOTWHAT_ORDER) ||
	    binfo->status > 1) {
		err = GRUB_ERR_NET_INVALID_RESPONSE;
		grub_printf("bootinfo: bogus bootinfo packet, ignored\n");
	} else {
		grub_memcpy(data->reply, binfo, plen);
		data->rlen = plen;
		data->stop = 1;
	}
	data->error = err;

	grub_netbuff_free(nb);
	return GRUB_ERR_NONE;
}

/*
 * Try to get a bootinfo response
 *
 * buffer: where to store the response
 * length: size of buffer
 * timeout: the time to wait for a response (in clock ticks),
 *          or 0 for polling mode
 *
 * This tries to approximate the behavior of having a socket with a receive
 * timeout by polling for approximately timeout clock ticks.
 *
 * returns zero if a packet was read.
 * 'length' will now contain the size of the packet.
 *
 * returns non-zero if no packet was read or an unknown error occurs.
 */
static int
get_response(struct recv_data *bdata, grub_uint32_t timeout)
{
	int rc = 0;
	grub_uint64_t start;

	start = grub_get_time_ms();
	do {
		/* We have to handle ESC events here too. Yuck. */
		if (get_key(1) == GRUB_TERM_ESC) {
			rc = USER_ABORT;
			break;
		}

		/* short poll for a packet */
		grub_net_poll_cards(1, &bdata->stop);

		if (bdata->stop != 0 && bdata->error == 0) {
#if 0
			print_packet(bdata->reply, bdata->rlen);
#endif
			return 0;
		}

		/* remember the last error in case we timeout */
		rc = bdata->error;

		bdata->error = 0;
		bdata->stop = 0;

		grub_millisleep(1);
	} while (!timeout || ((grub_get_time_ms() - start) < timeout));

	return (rc ? rc : GRUB_ERR_TIMEOUT);
}

static int
parse_linux_commandline(char *commandline)
{
	int rc = 0;
	char *p, *token;
	char *buffer = NULL;

	buffer = grub_malloc(grub_strlen(commandline) + 6 + 1);
	if (buffer == NULL) {
		rc = grub_errno;
		goto out;
	}

	grub_strcpy(buffer, commandline);
	p = grub_strchr(buffer, ' ');
	if (p) {
		*p++ = '\0';
	}
	if (*buffer) {
		grub_env_set("bootinfo_kernel", buffer);
	}

	if (p) {
		grub_env_set("bootinfo_kernel_args", p);

		token = grub_strstr(p, "initrd=");
		if (token) {
			p = grub_strchr(token, ' ');
			if (p) {
				*p = '\0';
			}
			grub_env_set("bootinfo_initrd", token + 7);
		}
	}

out:
	if (buffer)
		grub_free(buffer);

	return rc;
}

/* Set up environment variables so that we can pass them
 * to our modified FreeBSD loader or to a kernel that we
 * boot directly.
 *
 * Until we can load FreeBSD kernel modules, we boot a
 * FreeBSD system by loading our modified /boot/loader
 * that will load any environment passed in after parsing
 * loader.conf.  This lets us override image defaults with
 * the bootinfo commandline.
 */
static int
parse_freebsd_commandline(char *commandline)
{
	char *p, *token;
	char *value;
	char *path = NULL;
	char *kernel;
	char *buffer = NULL;
	int rc = 0;

	buffer = grub_malloc(grub_strlen(commandline) + 1);
	if (buffer == NULL) {
		rc = grub_errno;
		goto out;
	}
	grub_strcpy(buffer, commandline);

	path = buffer;
	p = buffer;
	while ((token = grub_strchr(p, ' '))) {
		*token = '\0';
		token++;
		p = token;

		if (*token == ' ') {
			continue;
		}

		p = grub_strchr(p, ' ');
		if (p) {
			*p = '\0';
		}

		value = grub_strchr(token, '=');
		if (value) {
			char *name;
			*value++ = '\0';
			name = grub_xasprintf("kFreeBSD.%s", token);
			grub_env_set(name, value);
			grub_free(name);
		}

		if (p) {
			*p++ = ' ';
		}
	}

	if (path[0] != '\0') {
		grub_env_set("bootinfo_kernel", path);

		/*
		 * For FBSD 5 and above, strip off leading "/boot/" as it will
		 * be assumed and will also screw up the setting of module_path
		 */
		if (grub_strncmp(path, "/boot/", 6) == 0) {
			path += 6;
		}

		/* Tell loader which kernel to boot and where to find it
		 * 	kernel		kernel directory from which to boot
		 * 	bootfile	kernel file to boot
		 */
		if (path[0] != '/' && (kernel = grub_strrchr(path, '/')) != 0) {
			*kernel++ = '\0';
			grub_env_set("kFreeBSD.bootfile", kernel);
			grub_env_set("kFreeBSD.kernel", path);
		} else {
			grub_env_set("kFreeBSD.bootfile", path);

			/*
			 * Such a hack: if the path started with slash (i.e., did not
			 * start with "/boot/"), then it is probably a 4.x boot where
			 * we need to set 'kernel' to the file to boot.
			 */
			if (path[0] == '/') {
				grub_env_set("kFreeBSD.kernel", path);
			}
		}
	}

out:
	if (buffer)
		grub_free(buffer);

	return rc;
}

/*
 * XXX hack function to work around shortcomings of UDP library:
 *  - doesn't allow setting of the in_port,
 *  - out_port can get changed in grub_net_recv_udp_packet()
 */
static void
grub_net_udp_set_ports(grub_net_udp_socket_t sock, int in_port, int out_port)
{
	/* mimic internal udp socket struct */
	struct foo {
		void *n, *p;
		enum { __START, __ESTABLISHED, __CLOSED } s;
		int in_port, out_port;
	} *mysock = (struct foo *)sock;

	if (mysock->s == __START) {
		mysock->in_port = in_port;
		mysock->out_port = out_port;
		mysock->s = __ESTABLISHED;
	} else {
		grub_printf("set_ports: socket not in the correct state!\n");
	}
}

static void
bootinfo_show(boot_what_t *boot_whatp)
{
	grub_printf("Default boot:\n");
	switch (boot_whatp->type) {

	case BIBOOTWHAT_TYPE_MB:
		grub_printf("boot multiboot kernel `%s:%s'",
			    ntoa(boot_whatp->what.mb.tftp_ip),
			    boot_whatp->what.mb.filename);
		break;

	case BIBOOTWHAT_TYPE_PART:
		grub_printf("boot from main disk, partition %d",
			    boot_whatp->what.partition);
		break;

	case BIBOOTWHAT_TYPE_DISKPART:
		grub_printf("boot from disk 0x%x, partition %d",
			    boot_whatp->what.dp.disk,
			    boot_whatp->what.dp.partition);
		break;

	case BIBOOTWHAT_TYPE_SYSID:
		grub_printf("boot from main disk partition with sysid %d",
			    boot_whatp->what.sysid);
		break;

	case BIBOOTWHAT_TYPE_WAIT:
		grub_printf("boot wait; waiting for bootinfo instructions\n");
		return;

	case BIBOOTWHAT_TYPE_MFS:
		grub_printf("boot from MFS %s", boot_whatp->what.mfs);
		break;

	default:
		grub_printf("!? invalid action %d\n", boot_whatp->type);
		return;
	}
	if (boot_whatp->cmdline[0])
		grub_printf(" with cmdline `%s'", boot_whatp->cmdline);
	grub_printf("\n");
}

static grub_err_t
grub_cmd_bootinfo (struct grub_extcmd_context *ctxt,
		   int argc __attribute__ ((unused)),
                   char **args __attribute__ ((unused)))
{
	struct grub_arg_list *state = ctxt->state;
	struct grub_net_buff *nb = NULL;
	grub_net_udp_socket_t sock = NULL, sock2 = NULL;
	boot_info_t *request, *reply;
	boot_what_t *boot_what;
	grub_net_network_level_address_t biserver;
	struct recv_data *bdata;
	in_addr_t bootinfo_ip;
	struct grub_net_network_level_interface *net;
	int rc = 0;
	char *bootinfo_name = NULL;
	char buf[32];
	int key;
	char *p;
	int showonly = 0;
	const char *dprefix = "hd";
	
	/* request allocated as part of packet struct */
	reply = grub_malloc(sizeof *reply);
	bdata = grub_malloc(sizeof *bdata);

	bootinfo_ip = 0;
	if (state[0].set) {
		bootinfo_name = state[0].arg;
	       	if (pton(state[0].arg, &bootinfo_ip) < 0) {
			rc = grub_error(GRUB_ERR_BAD_ARGUMENT,
					"invalid -s IP address");
			goto out;
		}
	}

	if (state[1].set) {
		showonly = 1;
	}

	if (state[2].set) {
		dprefix = state[2].arg;
		if (grub_strcmp(dprefix, "hd") &&
		    grub_strcmp(dprefix, "pata") &&
		    grub_strcmp(dprefix, "ahci")) {
			rc = grub_error(GRUB_ERR_BAD_ARGUMENT,
					"invalid -p prefix,"
					" must be one of: hd, pata, ahci");
			goto out;
		}
	}

	/* Extract our IP address from the cached DHCP data */
	if (bootinfo_ip == 0) {
		FOR_NET_NETWORK_LEVEL_INTERFACES(net) {
			if (net->dhcp_ack) {
				bootinfo_ip =
					find_bootinfo_ip(net->dhcp_ack,
							 net->dhcp_acklen);
				break;
			}
		}

		if (bootinfo_ip == 0) {
			rc = grub_error(GRUB_ERR_BUG,
					"Unable to find bootinfo address");
			goto out;
		}

		ntop(&bootinfo_ip, buf, sizeof(buf));
		bootinfo_name = buf;
	}

	/* make sure these are initialized before we open the socket */
	bdata->reply = reply;
	bdata->rlen = sizeof(*reply);
	bdata->stop = bdata->error = 0;

	/*
	 * Open UDP socket to server
	 */
	biserver.type = GRUB_NET_NETWORK_LEVEL_PROTOCOL_IPV4;
	biserver.ipv4 = bootinfo_ip;
	sock = grub_net_udp_open(biserver, BOOTWHAT_DSTPORT,
				 bootinfo_receive, bdata);
	if (!sock) {
		rc = grub_errno;
		goto out;
	}
	/* XXX udp_open assigns a random source port--no can do */
	grub_net_udp_set_ports(sock, BOOTWHAT_SRCPORT, BOOTWHAT_DSTPORT);

	/*
	 * We need a different socket for unsolicited bootinfosend
	 * packets which use a different source port.
	 */
	sock2 = grub_net_udp_open(biserver, BOOTWHAT_SENDPORT,
				  bootinfo_receive, bdata);
	if (!sock2) {
		rc = grub_errno;
		goto out;
	}
	/* XXX udp_open assigns a random source port--no can do */
	grub_net_udp_set_ports(sock2, BOOTWHAT_SRCPORT, BOOTWHAT_SENDPORT);

	/*
	 * Allocate a send buffer
	 */
	nb = grub_netbuff_alloc(GRUB_NET_MAX_LINK_HEADER_SIZE +
				GRUB_NET_OUR_MAX_IP_HEADER_SIZE +
				GRUB_NET_UDP_HEADER_SIZE +
				sizeof (*request));
	if (!nb) {
		rc = grub_error(GRUB_ERR_OUT_OF_MEMORY,
				"No memory for bootinfo packet");
		goto out;
	}

	grub_printf("Requesting bootinfo data from %s...", bootinfo_name);
	while (1) {
		grub_netbuff_clear(nb);
		grub_netbuff_reserve(nb, GRUB_NET_MAX_LINK_HEADER_SIZE +
				     GRUB_NET_OUR_MAX_IP_HEADER_SIZE +
				     GRUB_NET_UDP_HEADER_SIZE);
		grub_netbuff_put(nb, sizeof(*request));

		request = (boot_info_t *)nb->data;
		grub_memset(request, 0, sizeof(*request));
		request->version = BIVERSION_CURRENT;
		request->opcode  = BIOPCODE_BOOTWHAT_REQUEST;

		rc = grub_net_send_udp_packet(sock, nb);
		if (rc) {
			rc = grub_error(GRUB_ERR_IO,
					"failed to send bootinfo request");
			goto out;
		}

		grub_printf(".");

		grub_memset(reply, 0, sizeof(*reply));
		boot_what = (boot_what_t *)reply->data;

		/* Wait up to 10 seconds(-ish) for a response */
		boot_what->type = 0;
		bdata->rlen = sizeof(*reply);
		bdata->stop = bdata->error = 0;
		rc = get_response(bdata, 10000);
		if (rc) {
			grub_printf("\nerror %d while waiting for response\n",
				    rc);
			rc = grub_error(GRUB_ERR_IO,
					"failed to receive bootinfo response");
			goto out;
		}

		grub_printf(" ok\n");

		if (showonly) {
			bootinfo_show(boot_what);
			rc = 0;
			goto out;
		}

		while (boot_what->type == BIBOOTWHAT_TYPE_WAIT) {
			grub_printf("Polling for bootinfo response... ");
			boot_what->type = 0;
			bdata->rlen = sizeof(*reply);
			bdata->stop = bdata->error = 0;
			rc = get_response(bdata, 0);
			if (rc) {
				grub_printf("\nerror %d while waiting for response\n",
					    rc);
				rc = grub_error(GRUB_ERR_IO,
						"failed to receive bootinfo response");
				goto out;
			}
			grub_printf(" ok\n");
		}

		switch (boot_what->type) {
		case BIBOOTWHAT_TYPE_REBOOT:
			grub_printf("Rebooting...\n");
			return grub_command_execute("reboot", 0, NULL);
		case BIBOOTWHAT_TYPE_DISKPART:
			if (boot_what->what.dp.partition > 0) {
				grub_snprintf(buf, sizeof(buf), "%s%d,%d",
					      dprefix,
					      boot_what->what.dp.disk - 0x80,
					      boot_what->what.dp.partition);
			} else {
				grub_snprintf(buf, sizeof(buf), "%s%d",
					      dprefix,
					      boot_what->what.dp.disk - 0x80);
			}

			grub_env_set("bootinfo_orig_root", buf);

			if (boot_what->cmdline[0] != '\0') {
				grub_env_set("bootinfo_orig_cmdline", (const char *)boot_what->cmdline);
			}
			rc = GRUB_ERR_NONE;
			goto out;
		case BIBOOTWHAT_TYPE_PART:
			if (boot_what->what.partition > 0) {
				grub_snprintf(buf, sizeof(buf), "%s%d,%d",
					      dprefix, 0,
					      boot_what->what.partition);
			} else {
				grub_snprintf(buf, sizeof(buf), "%s%d",
					      dprefix, 0);
			}
			grub_env_set("bootinfo_orig_root", buf);

			if (boot_what->cmdline[0] != '\0') {
				grub_env_set("bootinfo_orig_cmdline", (const char *)boot_what->cmdline);
			}
			rc = GRUB_ERR_NONE;
			goto out;
		case BIBOOTWHAT_TYPE_MFS:
			if ((p = (char *)grub_env_get("root")) != NULL)
				grub_env_set("bootinfo_orig_root", p);
			if (boot_what->cmdline[0] != '\0') {
				grub_env_set("bootinfo_orig_cmdline", (const char *)boot_what->cmdline);
			}

			p = grub_strchr((const char *)boot_what->what.mfs, ':');
			if (p) {
				*p = '\0';
				grub_env_set("bootinfo_netboot_server", (const char *)boot_what->what.mfs);
				grub_env_set("bootinfo_netboot_path", p + 1);
				*p = ':';
			} else {
				grub_env_set("bootinfo_netboot_path", (const char *)boot_what->what.mfs);
			}

			rc = GRUB_ERR_NONE;
			goto out;
		case BIBOOTWHAT_TYPE_AUTO:
			grub_printf("query: will query again\n");
			continue;
		case BIBOOTWHAT_TYPE_SYSID:
			grub_printf("request to boot partition type 0x%02x not supported\n",
				    boot_what->what.sysid);
			break;
		default:
			rc = -1;
			break;
		}

		/* Only sleep if we got a bogus response or we are to
		 * go through the loop again.
		 *
		 * Use get_key() to do the sleep so we can catch ESC
		 * keypresses.
		 */
		if (rc) {
			grub_printf("Unexpected boot type %d\n",
				    boot_what->type);
			grub_printf("Retrying...");
			key = get_key(5 * 1000);
			if (key == GRUB_TERM_ESC) {
				grub_printf("\n");
				rc = GRUB_ERR_NONE;
				break;
			}
		}
	}

out:
	if (bdata)
		grub_free(bdata);
	if (reply)
		grub_free(reply);
	if (nb)
		grub_netbuff_free(nb);
	if (sock2)
		grub_net_udp_close(sock2);
	if (sock)
		grub_net_udp_close(sock);

	return rc;
}

static grub_err_t
grub_cmd_bootinfo_parse_cmdline (struct grub_extcmd_context *ctxt,
				 int argc, char **args)
{
	int rc = GRUB_ERR_NONE;
	struct grub_arg_list *state = ctxt->state;

	if (argc == 0) {
		return grub_error (GRUB_ERR_INVALID_COMMAND, "no argument specified");
	}

	if (state[0].set) {
		rc = parse_linux_commandline(args[0]);
	} else if (state[1].set) {
		rc = parse_freebsd_commandline(args[0]);
	}

	return (grub_err_t) rc;
}

#if 0
static void
print_ip (grub_uint32_t ip)
{
  int i;

  for (i = 0; i < 3; i++)
    {
      grub_printf ("%d.", ip & 0xFF);
      ip >>= 8;
    }
  grub_printf ("%d", ip);
}
#endif

static const struct grub_arg_option bootinfo_options[] =
{
	{"server", 's', 0, "IP address of bootinfo server", "SERVER", ARG_TYPE_STRING},
	{"noaction", 'n', 0, "just print what bootinfo returns", 0, 0},
	{"prefix", 'p', 0, "disk prefix to use, default is 'hd'", "PREFIX", ARG_TYPE_STRING},
	{0, 0, 0, 0, 0, 0}
};

static const struct grub_arg_option parse_cmdline_options[] =
{
	{"linux", 'l', 0, "parse arguments as Linux command line", 0, 0},
	{"freebsd", 'f', 0, "parse arguments as FreeBSD command line", 0, 0},
	{0, 0, 0, 0, 0, 0}
};

static grub_extcmd_t ecmd_1, ecmd_2;
GRUB_MOD_INIT(bootinfo)
{
  ecmd_1 = grub_register_extcmd ("bootinfo", grub_cmd_bootinfo, 0,
				 N_("[-n] [-s SERVER] [-p PREFIX]"),
				 N_("Get boot spec from Emulab"
				    " bootinfo server."),
				 bootinfo_options);

  ecmd_2 = grub_register_extcmd ("bootinfo_parse_cmdline", grub_cmd_bootinfo_parse_cmdline, 0,
				 N_("[-l|-f] COMMANDLINE"),
				 N_("Convert OS command line to"
				    " bootinfo-related variables."),
				 parse_cmdline_options);
}

GRUB_MOD_FINI(bootinfo)
{
  grub_unregister_extcmd (ecmd_1);
  grub_unregister_extcmd (ecmd_2);
}
