#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/err.h>
#include <grub/device.h>
#include <grub/disk.h>
#include <grub/fs.h>
#include <grub/normal.h>
#include <grub/partition.h>
#include <grub/msdos_partition.h>
#include <grub/gpt_partition.h>
#include <grub/extcmd.h>

GRUB_MOD_LICENSE ("GPLv3+");

#if 0
static void
gpt_guid_to_string (grub_gpt_part_type_t *type, char *string)
{
	grub_uint16_t data4;
	grub_uint32_t data5;
	grub_uint16_t data6;

	data4 = (type->data4[0] << 8) | type->data4[1];
	data5 = (type->data4[2] << 24) | (type->data4[3] << 16) |
		(type->data4[4] << 8) | type->data4[5];
	data6 = (type->data4[6] << 8) | type->data4[7];

	sprintf(string, "%08X-%04X-%04X-%04X-%08X%04X",
		grub_le_to_cpu32(type->data1),
		grub_le_to_cpu16(type->data2),
		grub_le_to_cpu16(type->data3),
		data4, data5, data6);
}
#endif

static grub_err_t
grub_cmd_devinfo (struct grub_extcmd_context *ctxt, int argc, char **args)
{
	struct grub_arg_list *state = ctxt->state;
	char *device;
	char *var = NULL;
	char *data = NULL;
	char buffer[256];
	grub_device_t dev = NULL;
	grub_fs_t fs;
	grub_err_t rc = GRUB_ERR_NONE;
	grub_partition_t part;

	if (argc == 0)
		return grub_error (GRUB_ERR_INVALID_COMMAND, "no argument specified");

	if (state[7].set) {
		var = state[5].arg;
	}

	device = args[0];

	grub_errno = GRUB_ERR_NONE;

	dev = grub_device_open (device);
	if (!dev) {
		rc = grub_errno;
		goto out;
	}

	fs = grub_fs_probe (dev);
	if (!fs) {
		rc = grub_errno;
		goto out;
	}

	if (state[0].set) {
		data = (char *)fs->name;
	} else if (state[1].set) {
		if (fs->label)
			(fs->label) (dev, &data);
	} else if (state[2].set) {
		if (fs->uuid)
			(fs->uuid) (dev, &data);
	} else if (state[3].set || state[4].set ||
                   state[5].set || state[6].set ) {
		if (!dev->disk || !dev->disk->partition) {
			rc = GRUB_ERR_BAD_PART_TABLE; /* is this right? */
			goto out;
		}

		part = dev->disk->partition;
		if (state[3].set) {
			data = (char *)part->partmap->name;
		} else if (state[4].set) {
			grub_uint32_t mstype = 256;

			if (!grub_strcmp(part->partmap->name, "msdos")) {
				mstype = part->msdostype;
			}
			else if (!grub_strcmp(part->partmap->name, "bsd")) {
				if (part->parent)
					mstype = part->parent->msdostype;
			}
			if (mstype < 256) {
				grub_snprintf(buffer, sizeof(buffer),
					      "%x", mstype);
				data = buffer;
			}
		}

	}

	if (data) {
		if (var)
			grub_env_set(var, data);
		else
			grub_printf("%s\n", data);
	}

out:
	if (dev)
		grub_device_close(dev);

	return rc;
}

static const struct grub_arg_option devinfo_options[] =
{
	{"fstype", 't', 0, "show filesystem type", 0, 0},
	{"label", 'l', 0, "show filesystem label", 0, 0},
	{"uuid", 'u', 0, "show filesystem uuid", 0, 0},
	{"pmap", 'M', 0, "show partition map type", 0, 0},
	{"ptype", 'T', 0, "show partition type", 0, 0},
#if 0
	{"puuid", 'U', 0, "show partition uuid", 0, 0},
	{"plabel", 'L', 0, "show partition label", 0, 0},
#endif
	{"set", 's', 0, "set a variable with the info returned instead of displaying it",
		"VAR", ARG_TYPE_STRING},
	{0, 0, 0, 0, 0, 0}
};

static grub_extcmd_t ecmd;
GRUB_MOD_INIT(devinfo)
{
  ecmd = grub_register_extcmd ("devinfo", grub_cmd_devinfo, 0,
			       N_("[-t|-l|-u|-p|-T|-s] DEVICE"),
			       N_("Display filesystem type, label,"
				  " UUID, partition table type,"
				  " and partition type for DEVICE."
				  " If --set is specified, the information"
				  " is stored in a variable instead of"
				  " displaying it."),
			       devinfo_options);
}

GRUB_MOD_FINI(devinfo)
{
  grub_unregister_extcmd (ecmd);
}
