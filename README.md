# Emulab modified version of grub2

This repository contains a modified version of Grub2 used to produce the
Emulab `grub2pxe` program. `grub2pxe` serves the same purpose for the
Linux-based MFSes as `pxeboot` does for the FreeBSD-based MFSes.

The changes here were originally made by Ryan Jackson in grub1.97~beta
back in 2009-10, but had to be heavily hacked due to changes in the structure
of Grub.

The repository's *grub2-source* branch contains a copy of the main Grub2
repo from Savannah.

## Instructions:

How to make Emulab grub2pxe (only tried on Ubuntu14 64-bit).

* First, in this source tree:

```
 ./autogen.sh
 ./configure
 make
 sudo make install
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2.0
```

* Copy `/tftpboot/grub2.0` contents over to `boss:/tftpboot/grub2.0`
* Copy `/tftpboot/grub2.0/i386-pc/core.0` to `/tftpboot/grub2.0/grub2pxe`
* Copy Emulab config file from `emulab/grub.cfg` to `/tftpboot/grub2.0/grub.cfg`

* Set a node's (or node_type's) `pxe_boot_path` attribute to
  `/tftpboot/grub2.0/grub2pxe`, run `dhcpd_makeconf -ir` and try it!

## Supporting different versions simultaneously:

We support two different parameters in our grub.cfg, native vs. BIOS disk
drivers and VGA vs. sio1 vs. sio2 for a console. To support different
combos, I make six different versions via `grub-mknetdir`:

```
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2pxe-bios-vga
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2pxe-bios-sio1
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2pxe-bios-sio2
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2pxe-native-vga
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2pxe-native-sio1
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2pxe-native-sio2
```

What needs to be different in each directory is just the grub2pxe
(aka, core.0) loader binary and the grub.cfg file. Everything else can
be symlinked to a common directory:

```
 4 drwxr-xr-x   2 root  root       512 Jul 27 12:42 ./
 4 drwxrwxr-x  76 root  tbadmin   3584 Jul 29 00:01 ../
 0 lrwxrwxrwx   1 root  root        16 Jul 27 12:39 fonts@ -> ../grub2.0/fonts
 8 -rw-r--r--   1 root  root      6066 Jul 27 12:42 grub.cfg
44 -rw-r--r--   1 root  root     44047 Jul 27 12:37 grub2pxe
 0 lrwxrwxrwx   1 root  root        18 Jul 27 12:39 i386-pc@ -> ../grub2.0/i386-pc
 0 lrwxrwxrwx   1 root  root        17 Jul 27 12:39 locale@ -> ../grub2.0/locale
```

and just install the common stuff with:

```
 sudo grub-mknetdir --net-directory=/tftpboot --subdir=grub2.0
```

## Supporting EFI

 * For the configure command do:

```
 ./configure --with-platform=efi
```

The install and `grub-mknetdir` processes are the same. The differences are:

 * when installing on boss you need to copy `x86_64-efi/core.efi` to
   `grub2pxe`,

 * in the version-specific directory you should link `x86_64-efi`
   to `../grub2.0/x86_64-efi` instead of linking `i386-pc`.

## Updating the *grub2-source* branch from GNU

```
git checkout grub2-source
git fetch grub2-source
git merge grub2-source/master
```
